package com.bg.ds.entity.log;

import lombok.Data;

@Data
public class UserScanLog {
    private Long channelId;//频道id;
    private Long categoryId;//产品类别id
    private Long productId;//产品id
    private String contry;//国家
    private String province;//省份
    private String city;//城市
    private String network;//网络方式
    private String sources;//来源方式
    private String broswerType;//浏览器类型
    private Long starttime;//打开时间
    private Long endetime;//离开时间
    private Long userId;//用户id
}
