package com.bg.dsinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * 数据上报
 */
@Controller
@RequestMapping(value = {"/dsInfo"})
public class DsInfoReportController {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @PostMapping(value = {"/report"})
    public void webInfoRepot(@RequestBody String json, HttpServletRequest req, HttpServletResponse res) {
        System.out.println("数据上报中：" + json);
        kafkaTemplate.send("ds",String.valueOf(System.currentTimeMillis()),json);
        //业务开始
        //业务结束
        res.setStatus(HttpStatus.OK.value());
        PrintWriter wr = getPrintWrite(res);
        wr.write("success");
        closePrintWrite(wr);
    }

    private PrintWriter getPrintWrite(HttpServletResponse res) {
        res.setCharacterEncoding("utf-8");
        res.setContentType("application/json");
        OutputStream out = null;
        PrintWriter writer = null;
        try {
            out = res.getOutputStream();
            writer = new PrintWriter(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    private void closePrintWrite(PrintWriter writer) {
        if (writer != null) {
            writer.flush();
            writer.close();
        }
    }
}
