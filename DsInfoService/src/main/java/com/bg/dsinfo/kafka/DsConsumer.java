package com.bg.dsinfo.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * kafka消费者测试
 */
@Component
public class DsConsumer {

    @KafkaListener(topics = "ds")
    public void listen(ConsumerRecord<String, String> record) throws Exception {
        System.out.printf("topic = %s, offset = %d, key = %s,value = %s \n", record.topic(), record.offset(), record.key(), record.value());
    }
}
