package com.bg.dsinfo.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 数据上报工具
 */
public class DataReportTool {

    private DataReportTool() {
    }

    /**
     * 数据上报的方法
     * @param address
     * @param msg
     */
    public static void report(String address, String msg) {
        try {
            URL url = new URL(address);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            //重定向
            conn.setAllowUserInteraction(true);
            //关闭缓存
            conn.setUseCaches(false);
            //设置超时时间
            conn.setReadTimeout(6 * 1000);
            //模拟客户端请求
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
            //请求类型
            conn.setRequestProperty("Content-Type", "application/json");
            //连接
            conn.connect();

            //写出数据
            OutputStream out = conn.getOutputStream();
            BufferedOutputStream bufferOut = new BufferedOutputStream(out);
            bufferOut.write(msg.getBytes());
            bufferOut.flush();

            //返回信息
            StringBuilder builder = new StringBuilder();
            String line = "";
            InputStream in = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            System.out.println(conn.getResponseCode());
            System.out.println(builder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
