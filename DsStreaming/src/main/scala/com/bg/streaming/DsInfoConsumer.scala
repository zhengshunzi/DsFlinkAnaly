package com.bg.streaming

import com.alibaba.fastjson.JSON
import com.bg.ds.entity.log.UserScanLog
import com.bg.dss.utils.RedisUtil
import org.apache.flink.streaming.api.scala._
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.{CheckpointingMode, TimeCharacteristic}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010
import org.slf4j.LoggerFactory
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.api.common.restartstrategy.RestartStrategies

object DsInfoConsumer {
  def main(args: Array[String]): Unit = {
    val args = Array[String](
      "--input-topic", "ds",
      "--bootstrap.servers", "127.0.0.1:9092",
      "--zookeeper.connect", "127.0.0.1:2181"
    )

    // parse input arguments
    val parameterTool = ParameterTool.fromArgs(args)

    if (parameterTool.getNumberOfParameters < 3) {
      System.out.println("Missing parameters!\n" + "Usage: Kafka --input-topic <topic> --output-topic <topic> " + "--bootstrap.servers <kafka brokers> " + "--zookeeper.connect <zk quorum> --group.id <some id>")
      return
    }

    val logger = LoggerFactory.getLogger(this.getClass);
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //默认情况下，检查点被禁用。要启用检查点，请在StreamExecutionEnvironment上调用enableCheckpointing(n)方法，
    // 其中n是以毫秒为单位的检查点间隔。每隔5000 ms进行启动一个检查点,则下一个检查点将在上一个检查点完成后5秒钟内启动
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.enableCheckpointing(1000)
    env.getCheckpointConfig.setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE)
    env.getConfig.disableSysoutLogging
    env.getConfig.setRestartStrategy(RestartStrategies.fixedDelayRestart(4, 10000))
    env.getConfig.setGlobalJobParameters(parameterTool) // make parameters available in the web interface
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val properties = parameterTool.getProperties
    //    val properties = new Properties()
    //    properties.setProperty("bootstrap.servers", "127.0.0.1:9092")
    //    // only required for Kafka 010
    //    properties.setProperty("zookeeper.connect", "127.0.0.1:2181")
    properties.setProperty("group.id", "something")
    properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")

    val stream = env.addSource(new FlinkKafkaConsumer010[String](parameterTool.get("input-topic"), new SimpleStringSchema(), properties))
    stream
      .filter(_ != null)
      .map(line => {
        val userScanLog = JSON.parseObject(line, classOf[UserScanLog])
        (userScanLog.getUserId, userScanLog.getChannelId, 1)
      })
      //根据用户id和频道id分组
      .keyBy(k => {
      (k._1, k._2)
    })
      //求两个出现的次数
      .sum(2)
      //1个线程计算
      .setParallelism(1)
      //存入到redis
      .addSink(x => {
      println(x.toString + "-------------------------------")
      val jedis = RedisUtil.getJedisResource();
      jedis.lpush("ds".getBytes("UTF-8"), x.toString().getBytes("UTF-8"))
      jedis.close()
    }
    ).name("dsInfo")
    env.execute("DsInfoConsumer")
  }
}
