package com.bg.test.socket

import org.apache.flink.streaming.api.scala._

object WindowWordCount {
  def main(args: Array[String]) {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val text = env.socketTextStream("localhost", 9999)

    //    val counts = text.flatMap { _.toLowerCase.split("\\W+") filter { _.nonEmpty } }
    //      .map { (_, 1) }
    //      .keyBy(0)
    //      //.timeWindow(Time.seconds(5))
    //      .sum(1)

    val counts = text.map(line => {
                val array = line.split("\\W+")
                (array(0), array(1), 1)
                })
      //按多个字段分组
      .keyBy(0, 1)
      .sum(2)

    counts.print()
    env.execute("Window Stream WordCount")
  }
}
