package com.bg.dsv.controller;

import com.bg.dsv.service.DsChannelHotSpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/ds"})
public class DsChannelHotSpotController {

    @Autowired
    private DsChannelHotSpotService dsChannelHotSpotService;

    @GetMapping(value = {"/list"})
    public String getLatelyChannelHotSpot(Model model) {
        System.out.println("ds查询");
        String value = dsChannelHotSpotService.getKey("a");
        model.addAttribute("ll", value);
        return "/hello";
    }
}
