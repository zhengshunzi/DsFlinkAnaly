package com.bg.dsv.service.impl;

import com.alibaba.fastjson.JSON;
import com.bg.ds.entity.log.UserScanLog;
import com.bg.dsv.service.DsChannelHotSpotService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/*
    redisTemplate.opsForValue();　　//操作字符串
    redisTemplate.opsForHash();　　 //操作hash
    redisTemplate.opsForList();　　 //操作list
    redisTemplate.opsForSet();　　  //操作set
    redisTemplate.opsForZSet();　 　//操作有序set
 */
@Service(value = "DsChannelHotSpotService")
public class DsChannelHotSpotServiceImpl implements DsChannelHotSpotService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String getKey(String key) {
        List<String> ds = stringRedisTemplate.opsForList().range("ds", 0, -1);
        ds.forEach(l -> {
            UserScanLog userScanLog = JSON.parseObject(l, UserScanLog.class);
            System.out.println(userScanLog.toString());
        });
        String value = stringRedisTemplate.opsForValue().get("a");
        return String.valueOf(value);
    }
}
