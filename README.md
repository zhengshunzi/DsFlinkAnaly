# DsFlinkAnaly

#### 介绍
Flink基于电商数据的实时计算处理

#### 软件架构
模拟客户端上报数据-->controller对数据进行处理-->上报kafka-->flink实时处理数据


#### 安装教程

1. jdk8
2. maven
3. zookeeper
4. kafka
5. Flink
6. mysql
7. hbase
#### 使用说明

1. 启动springboot项目DsInfoService
2. 启动DsInfoService项目中的test模拟上报数据
3. 启动DsStreaming对kafka中的数据进行处理

#### 参与贡献
zhengshunzhi
